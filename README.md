## RyRa

[RyRa online](https://arthur.sw.gitlab.io/ryra/)

A random rythm generator

### Usage

Select the rythms you want to use and RyRa will generate 4 random bars from selected rythms.

Made with [VexFlow](http://www.vexflow.com/).